package com.jansing;

import java.util.Objects;

/**
 * Created by Administrator on 2016/3/24.
 */
public class First {
    public Integer n;
    public int add(int x, int y){
        return x+y;
    }

    @Override
    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        First first = (First) o;
//        return Objects.equals(n, first.n);
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(n);
    }

    /**
     * main方法测试弊端：
     * 1、不能对每个方法进行单独测试
     * 2、不能自动化检查方法执行结果的正确性
     * @param args
     */
    public static void main(String[] args) {
        int x = new First().add(3, 5);
        System.out.println(x);
    }
}
