package com.jansing;

import org.junit.*;

import java.util.*;

import static org.hamcrest.Matchers.*;

import static org.junit.Assert.*;

/**
 * Ignore、
 *
 *
 * Created by Administrator on 2016/3/24.
 */
public class FirstTest {
    private int x = 10;
    private static int y = 10;

    //BeforeClass、AfterClass在所有测试前后执行，即只执行一次，方法必须static
    @BeforeClass
    public static void beforeClass(){
        System.out.println("beforeClass");
        y = 11;
    }

    @AfterClass
    public static void afterClass(){
        System.out.println("afterClass");
    }

    //Before、After：每个测试方法前后都会执行，执行多次
    @Before
    public void before(){
        System.out.println("before");
        System.out.println("y=" + y);
    }
    @After
    public void after(){
        System.out.println("after");
    }


    @Test
    public void testEquals() throws Exception {
        int x = new First().add(3, 5);
        assertEquals(x, 8);
        //如果出错，作为提示信息出现在结果中
        assertEquals("x = " + x, x, 8);
    }

    @Test
    public void testIs(){
        int x = new First().add(3, 5);
        //使用assertThat替代其他所有assert
        assertThat(x, is(8));
    }

    @Test
    public void testNot(){
        int x = new First().add(3, 5);
        //使用assertThat替代其他所有assert
        assertThat(x, not(9));
    }

    @Test
    public void testNull(){
        assertThat(null, nullValue());
        assertThat(x,  notNullValue());
    }

    @Test
    public void testThan(){
        //大于
        assertThat(x, greaterThan(9));
        //大于或等于
        assertThat(x, greaterThanOrEqualTo(10));
        //等于
        assertThat(x, equalTo(10));
        //小于
        assertThat(x, lessThan(11));
        //小于或等于
        assertThat(x, lessThanOrEqualTo(10));
    }

    @Test
    public void testAllOf(){
        //满足所有条件
        assertThat(x, allOf(greaterThan(9), lessThan(11)));
    }

    @Test
    public void testAnyOf(){
        //只需满足一个条件
        assertThat(x, anyOf(greaterThan(10), equalTo(10)));
    }
    @Test
    public void testAnything(){
        //任何情况都成立
        assertThat(null, anything());
    }

    @Test
    public void testObject(){
        Number n = new Integer(256);
        //为其或其子类的实例
        assertThat(n, instanceOf(Integer.class));

    }

    @Test
    public void testString(){
        String tmp = "";
        assertThat(tmp, isEmptyString());
        assertThat(tmp, isEmptyOrNullString());
        String s = new String("jansing");
        assertThat(s, containsString("jan"));
        assertThat(s, startsWith("jan"));
        assertThat(s, endsWith("ng"));
        assertThat(s, equalToIgnoringCase("JANSING"));
        //忽略头尾空格
        assertThat(s, equalToIgnoringWhiteSpace(" jansing"));

        //list中的元素按s的顺序
        List<String> list = new ArrayList<>();
        list.add("s");
        list.add("n");
        assertThat(s, stringContainsInOrder(list));

    }

    @Test
    public void testArray(){
        Integer[] array = new Integer[4];
        //判断array.length=0，不论是否为null
//        assertThat(array, emptyArray());
        array[0] = 128;
        array[1] = 256;
        array[2] = 512;
        assertThat(array, hasItemInArray(128));
        //null也要包括在内
        assertThat(array, arrayContaining(128, 256, 512, null));
        assertThat(array, arrayContainingInAnyOrder(null, 128, 512, 256));

        assertThat(128, isIn(array));
    }

    @Test
    public void testList(){
        List<Integer> list = new ArrayList<>();
        //集合为空
        assertThat(list, empty());
        assertThat(list, emptyCollectionOf(Integer.class));
        assertThat(list, emptyIterable());
        assertThat(list, emptyIterableOf(Integer.class));
        list.add(128);
        list.add(256);
        list.add(512);
        list.add(null);
        //集合有几个元素
        assertThat(list, hasSize(4));

        //集合中必有某个/些元素
        assertThat(list, hasItem(128));
        assertThat(list, hasItems(128, 256));
        //完全有序匹配，即list中的所有元素顺序一致
        assertThat(list, contains(128, 256, 512, null));
        assertThat(list, containsInAnyOrder(null, 128, 512, 256));


        assertThat(128, isIn(list));
        //是参数列表中的一个，一个参数即为一个整体，在这里n1=128=n2<>list
        assertThat(128, isOneOf(list, 128));
    }


    @Test
    public void testSet(){
        Set<Integer> set = new HashSet<>();
        //集合为空
        assertThat(set, empty());
        assertThat(set, emptyCollectionOf(Integer.class));
        assertThat(set, emptyIterable());
        assertThat(set, emptyIterableOf(Integer.class));
        set.add(128);
        set.add(256);
        set.add(512);
        set.add(null);
        //集合有几个元素
        assertThat(set, hasSize(4));

        //集合中必有某个/些元素
        assertThat(set, hasItem(128));
        assertThat(set, hasItems(128, 256));
        //完全有序匹配，即list中的所有元素顺序一致
        assertThat(set, contains(128, 256, 512, null));
        assertThat(set, containsInAnyOrder(null, 128, 512, 256));


        assertThat(128, isIn(set));
        //是参数列表中的一个，一个参数即为一个整体，在这里n1=128=n2<>list
        assertThat(128, isOneOf(set,128));
    }

    @Test
    public void testMap(){
        Map<String, String> map = new HashMap<>();
        map.put("jan", "123");
        map.put("ans", "234");
        map.put("nsi", "345");

        assertThat(map, hasEntry("jan", "123"));
        assertThat(map, hasKey("jan"));
        assertThat(map, hasValue("123"));
    }

    //发生异常并且在100ms内完成
    @Test(expected=java.lang.ArithmeticException.class, timeout = 100)
    public void testException() throws InterruptedException {
        Thread.sleep(50);
        int i = 3/0;
    }
}