/**
 * 实现/覆盖 方法，只需方法签名一致即可，不包括声明的异常
 * Created by jansing on 2016/8/2.
 */
public class Third extends ThirdBaseClass implements ThirdInterface{


    @Override
    public void f() {
    }
}
