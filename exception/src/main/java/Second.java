/**
 * 如果finally中抛出异常，或使用return语句，
 * 则会导致异常丢失
 * Created by jansing on 2016/8/2.
 */
public class Second {
    public static void f() throws VeryImportantException {
        throw new VeryImportantException();
    }
    public static void g(){
        throw new HoHumException();
    }

    public static void main(String[] args){
            try {
                try {
                    f();
                } finally {
                    System.out.println("innerfinally");
                    return ;
//                    g();
                }
            } catch (Exception e) {
                //如果finally抛出异常，则输出HoHumException
                //如果是return，则catch语句不执行
                System.out.println(123);
                System.out.println(e);
            }
    }

}

class VeryImportantException extends Exception{}
class HoHumException extends RuntimeException{}
