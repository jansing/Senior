/**
 * e.fillInStackTrace();表示把e的类型与提示信息作为新的异常抛出，保留e的类型、提示信息
java.lang.NullPointerException: exception message
    at First.g(First.java:29)
    at First.main(First.java:36)
    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
    at java.lang.reflect.Method.invoke(Method.java:497)
    at com.intellij.rt.execution.application.AppMain.main(AppMain.java:140)
 *
 * new RuntimeException(e);相当于把e作为因子构建新的异常，保留e的抛出位置、类型、提示信息
java.lang.RuntimeException: java.lang.NullPointerException: exception message
    at First.g(First.java:15)
    at First.main(First.java:21)
    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
    at java.lang.reflect.Method.invoke(Method.java:497)
    at com.intellij.rt.execution.application.AppMain.main(AppMain.java:140)
Caused by: java.lang.NullPointerException: exception message
    at First.f(First.java:7)
    at First.g(First.java:11)
    ... 6 more

 * Created by jansing on 2016/8/1.
 */
public class First {
    public static void f(){
        throw new NullPointerException("exception message");
    }
    public static void g() throws Throwable {
        try {
            f();
        }catch (NullPointerException e){
            e.printStackTrace(System.out);
            throw e.fillInStackTrace();
//            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args){
        try {
            g();
        } catch (Throwable throwable) {
            throwable.printStackTrace(System.out);

        }
    }
}
