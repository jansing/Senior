package jxl;

import jxl.read.biff.BiffException;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import org.junit.Test;

import java.io.*;

/**
 * 09年停止了更新。
 * 与poi对比：
 * 操作简单，但仅支持97-03版本，且功能较少。
 * Created by Administrator on 2016/3/22.
 */
public class ReadTest {

    @Test
    public void test01() throws IOException, BiffException, WriteException {

        InputStream is = new FileInputStream("test03.xls");
        readExcel(is);
        is.close();
    }


    /**
     * 兼容03
     * 95乱码
     * 不兼容2013
     * @param is
     * @throws WriteException
     * @throws IOException
     * @throws BiffException
     */
    public void readExcel(InputStream is) throws WriteException, IOException, BiffException {
        //创建工作薄
        Workbook workbook1 = Workbook.getWorkbook(is);
        Sheet sheet1 = workbook1.getSheet(0);

        for(int i=0; i<4; i++){
            for(int j=0; j<3; j++){
                Cell cell = sheet1.getCell(j, i);
                System.out.print(cell.getContents()+"\t\t");
//                System.out.println(cell.getRow()+" - "+cell.getColumn()+" - "+cell.getCellFeatures()+" - "+cell.getCellFormat()+" - "+cell.getContents()+" - "+cell.getType());
//                System.out.println();

            }
            System.out.println();
        }

    }



}
