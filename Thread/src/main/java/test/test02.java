package test;

import java.util.concurrent.TimeUnit;

/**
 * 进制转换
 * 小进制转换成大进制会丢失精度，结果仍为整数
 * Created by jansing on 2015/12/2.
 */
public class test02 {
    public static void main(String[] args){
        //纳秒
        TimeUnit nano = TimeUnit.NANOSECONDS;
        //微秒
        TimeUnit micro = TimeUnit.MICROSECONDS;
        //毫秒
        TimeUnit milli = TimeUnit.MILLISECONDS;
        //秒
        TimeUnit second = TimeUnit.SECONDS;
        //分
        TimeUnit minute = TimeUnit.MINUTES;
        //时
        TimeUnit hour = TimeUnit.HOURS;
        //天
        TimeUnit day = TimeUnit.DAYS;

        convert(1, minute);
        convert(1, nano, micro);
    }


    public static void convert(long srcTime, TimeUnit srcMeasure){
        System.out.println(srcMeasure.toMillis(srcTime));
    }

    public static void convert(long srcTime, TimeUnit srcMeasure, TimeUnit destMeasure){
        System.out.println(destMeasure.convert(srcTime, srcMeasure));
    }
}
