package test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by jansing on 2015/12/2.
 */
public class test01 implements Runnable{
    private int id;
    private static int a = 0;

    public test01(int id){
        this.id = id;
    }

    public static synchronized int next(){
        Thread.yield();
        a++;
        Thread.yield();
        test();
        return a;
    }

    //锁中锁
    public static synchronized void test(){
        System.out.println("inner lock");
    }

    public static void main(String[] args){
        //not the one test01;
        ExecutorService exec = Executors.newCachedThreadPool();
        for(int i=0; i<10; i++){
            //注意，static synchronized 可以起作用，run中不同对象无法同时进入这个方法
            exec.execute(new test01(i));
        }
    }

    @Override
    public void run() {
        while(a<100){
            System.out.println(id + ", a = " + test01.next());
            Thread.yield();
        }
    }
}
