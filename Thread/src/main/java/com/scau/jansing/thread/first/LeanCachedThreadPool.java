package com.scau.jansing.thread.first;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by jansing on 2015/12/1.
 */
public class LeanCachedThreadPool {
    public static void main(String[] args){
        //根据需要创建线程，如果线程在60s内未被使用，则销毁
        ExecutorService threadPool = Executors.newCachedThreadPool();
        for(int i=0; i<5; i++){
            threadPool.execute(new LiftOff());
        }
        //线程池不再接受新线程
        threadPool.shutdown();
    }
}
