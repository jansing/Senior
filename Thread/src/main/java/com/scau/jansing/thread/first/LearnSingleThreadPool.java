package com.scau.jansing.thread.first;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by jansing on 2015/12/1.
 */
public class LearnSingleThreadPool {
    public static void main(String[] args){
        //当前任务结束前不会执行下一个任务
        ExecutorService threadPool = Executors.newSingleThreadExecutor();
        for(int i=0; i<5; i++){
            threadPool.execute(new LiftOff());
        }
        //线程池不再接受新线程
        threadPool.shutdown();
    }
}
