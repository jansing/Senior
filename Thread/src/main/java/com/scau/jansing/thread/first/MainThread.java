package com.scau.jansing.thread.first;

/**
 * Created by jansing on 2015/12/1.
 */
public class MainThread {

    public static void main(String[] args){
//        for(int i=0; i<2; i++) {
            Thread t = new Thread(new LiftOff(10));
            t.start();
//        }
        System.out.println("waiting for LiftOff");
        while(t.isAlive()){
            System.out.println("alive");
            System.out.println(t.getState());
            Thread.yield();
        }
        System.out.println("dead");
        System.out.println(t.getState());

    }
}
