package com.scau.jansing.thread.first;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by jansing on 2015/12/1.
 */
public class LearnFixedThreadPool {
    public static void main(String[] args){
        //固定线程池，超出个数限制的将等待执行
        //sleep的线程仍然占用一个资源
        ExecutorService threadPool = Executors.newFixedThreadPool(3);
//        sumbit 与 execute 比，更优先
//        threadPool.submit(new TaskWithResult(1));
//        for(int i=0; i<5; i++){
//            threadPool.execute(new LiftOff());
//        }

        //测试后台线程========================

//        后台线程如果是加入到线程池进行管理，则在非后台线程完成并over后，不会over，必须是执行.start();才会自动结束。并且是直接结束,没有异常
        Thread t = new Thread(new LiftOff(99));
        t.setDaemon(true);
//        threadPool.execute(t);
        t.start();
        t.interrupt();
        for(int i=0; i<5; i++){
            threadPool.execute(new LiftOff());
        }
//        ============================


        //线程池不再接受新线程
        threadPool.shutdown();
    }
}
