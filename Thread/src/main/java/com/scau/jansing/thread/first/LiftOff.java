package com.scau.jansing.thread.first;

import java.util.concurrent.TimeUnit;

/**
 * Created by jansing on 2015/12/1.
 */
public class LiftOff implements Runnable {

    protected int countDown = 10;
    private static int taskCount = 0;
    private final int id = taskCount++;


    public LiftOff(){
    }

    public LiftOff(int countDown){
        this.countDown = countDown;
    }

    public String status(){
        return "#" + id + "(" +
                (countDown > 0 ? countDown : "LiftOff") + ").";
    }

    @Override
    public void run() {
        try {
            while (countDown-- > 0) {
                System.out.println(status());
                Thread.yield();
            }
        }catch (Exception e){
            System.out.println(e);
        }finally {
            System.out.println("is run? " + id);
        }
    }
}
