package com.scau.jansing.thread.first.latch;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

/**
 * 如果希望创建一组任务，他们并行的执行工作，然后再进行下一个步骤之前等待，直至所有任务都完成。
 * 它使得所有的并行任务都将在栅栏处列队，因此可以一直的向前移动。
 * CountDownLatch是只触发一次的时间，而CyclicBarrier可以多次重用。
 * Created by jansing on 2016/1/1.
 */
public class LearnCyclicBarrier {
    public static void main(String[] args) {
        int nHorses = 7;
        int pause = 200;
        new HorseRace(nHorses, pause);
    }

}

class Horse implements Runnable {
    private static int count = 0;
    private final int id = count++;
    private int strides = 0;
    private static Random random = new Random(47);
    private static CyclicBarrier barrier;

    public Horse(CyclicBarrier cyclicBarrier) {
        barrier = cyclicBarrier;
    }

    public synchronized int getStrides() {
        return strides;
    }

    @Override
    public void run() {
        try {
            while (!Thread.interrupted()) {
                synchronized (this) {
                    strides += random.nextInt(3)+1;
                }
                barrier.await();                    //important
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "Horse " + id + " ";
    }

    public String tracks() {
        StringBuilder s = new StringBuilder();
        int strides = getStrides();
        for (int i = 0; i < strides; i++) {
            s.append("*");
        }
        s.append(id);
        return s.toString();
    }
}

class HorseRace {
    static final int FINISH_LINE = 75;
    private List<Horse> horses = new ArrayList<>();
    private ExecutorService exec = Executors.newCachedThreadPool();
    private CyclicBarrier barrier;

    public HorseRace(int nHorse, final int pause) {
//        CyclicBarrier(总数,当await数达到总数则调用)
        barrier = new CyclicBarrier(nHorse, new Runnable() {        //important
            @Override
            public void run() {
                StringBuilder s = new StringBuilder();
                for (int i = 0; i < FINISH_LINE; i++) {
                    s.append("=");
                }
                System.out.println(s.toString());
                for (Horse horse : horses) {
                    System.out.println(horse.tracks());
                }
                boolean finishFlag = false;
                for (Horse horse : horses) {
                    if (horse.getStrides() >= FINISH_LINE) {
                        finishFlag = true;
                        System.out.println(horse + "won");
                    }
                }
                if(finishFlag){
                    exec.shutdownNow();
                    return;
                }
                try {
                    TimeUnit.MILLISECONDS.sleep(pause);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        for (int i = 0; i < nHorse; i++) {
            Horse horse = new Horse(barrier);
            horses.add(horse);
            exec.execute(horse);
        }
    }
}
