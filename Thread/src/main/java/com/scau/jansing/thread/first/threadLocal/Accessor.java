package com.scau.jansing.thread.first.threadLocal;

/**
 * Created by jansing on 2015/12/2.
 */
public class Accessor implements Runnable {
    private final int id;

    public Accessor(int id) {
        this.id = id;
    }

    @Override
    public void run() {
        while(!Thread.currentThread().isInterrupted()){
            ThreadLocalVariableHolder.increment();
            System.out.println(this);
            Thread.yield();
        }
    }

    public String toString(){
        return "#" + id + ": " + ThreadLocalVariableHolder.get();
    }
}
