package com.scau.jansing.thread.first.resource;

import sun.nio.cs.Surrogate;

/**
 * Created by jansing on 2015/12/2.
 */
public abstract class IntGenerator {
    private volatile boolean canceled = false;
    public abstract int next();
    public void cancel(){
        canceled = true;
    }
    public boolean isCanceled(){
        return canceled;
    }
}
