package com.scau.jansing.util;

import com.scau.jansing.work.Launch;
import junit.framework.TestCase;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by jansing on 2015/10/24.
 */
public class CommonUtilTest extends TestCase {


    @Test
    public void test01() throws IOException {
        String path = System.getProperty("user.dir") + File.separator + "src\\main\\resources\\Test.java";
        File file = new File(path);
        BufferedReader bf = new BufferedReader(new FileReader(file));
        String str = bf.readLine();
        bf.close();
        System.out.println(str);
        System.out.println(CommonUtil.remove01(str));
    }
    @Test
    public void test02() throws IOException {
        String path = System.getProperty("user.dir") + File.separator + "src\\main\\resources\\Test.java";
        File file = new File(path);
        BufferedReader bf = new BufferedReader(new FileReader(file));
        String str = bf.readLine();
        bf.close();
        System.out.println(str);
        System.out.println(CommonUtil.remove02(str));
    }

    @Test
    public void test03() throws IOException {
        String path = System.getProperty("user.dir") + File.separator + "src\\main\\resources\\Test.java";
        File file = new File(path);
        BufferedReader bf = new BufferedReader(new FileReader(file));
        String str;
        while((str = bf.readLine()) != null){
            System.out.println("----->" + str);
            System.out.println(CommonUtil.removeInvalid(str, bf));
        }
        bf.close();
    }

    @Test
    public void test04(){
        Map<String, Integer> map = new HashMap<>();
        Integer i = map.get("fds");
        System.out.println(i);
    }

    @Test
    public void test05() throws IOException {
        String path = System.getProperty("user.dir") + File.separator + "src\\main\\resources\\Test.java";
        File file = new File(path);
        BufferedReader bf = new BufferedReader(new FileReader(file));
        System.out.println(bf.read());
        bf.close();
    }

    @Test
    public void test06() throws IOException {
        String path = System.getProperty("user.dir") + File.separator + "src\\main\\resources\\keywords.txt";
        File file = new File(path);
        Launch.getKeywords(file);
    }

    @Test
    public void test07() throws IOException {
        String path = System.getProperty("user.dir") + File.separator + "src\\main\\resources\\Test.java";
        File file = new File(path);
        String keywordsPath = System.getProperty("user.dir") + File.separator + "src\\main\\resources\\keywords.txt";
        File keywordsFile = new File(keywordsPath);
        Launch.analysis(file, keywordsFile);
    }

    @Test
    public void test08(){
        List<Integer> list = new ArrayList<>();
        Integer integer1 = 1;
        Integer integer2 = 2;
        list.add(integer1);
        list.add(integer2);

        Integer i = list.get(0);
        System.out.println(i);
        i = i + list.get(1);
        System.out.println(i);
        System.out.println(list.get(0));
    }

    @Test
    public void test09(){
        List<String> list = new ArrayList<>();
        String str1 = "1";
        String str2 = "2";
        list.add(str1);
        list.add(str2);
        String str = list.get(0);
        System.out.println(str);

        str = str + str2;
        System.out.println(str);
        System.out.println(list.get(0));
    }

    @Test
    public void test10(){
        String path = "log/today.log";
        System.out.println(path.substring(path.lastIndexOf('/')));
        System.out.println(path.substring(0,path.lastIndexOf('/')));
        System.out.println(path.substring(0,path.lastIndexOf('/')+1));

    }
}