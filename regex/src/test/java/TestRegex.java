import com.scau.jansing.util.CommonUtil;
import org.junit.Test;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.MatchResult;

/**
 * Created by jansing on 2015/10/21.
 */
public class TestRegex {


    @Test
    public void test01(){
        String string = "<li>    <a href=\"http://www.access-company.com/\" rel=\"nofollow\" class=\"member\">ACCESS CO., LTD.</a>          </li>  \n" +
                "<li>    <a href=\"http://www.accessibility.nl\" rel=\"nofollow\" class=\"member\">Accessibility Foundation</a>\n" +
                "\t\t\t— <a  class=\"testimonial_link\" href=\"http://www.w3.org/Consortium/Member/Testimonial/#t745\" title=\"Accessibility Foundation Member testimonial\">testimonial</a>      </li>  \n" +
                "<li>    <a href=\"http://www.adesis.com\" rel=\"nofollow\" class=\"member\">Adesis Netlife S.L.</a>\n" +
                "\t\t\t— <a  class=\"testimonial_link\" href=\"http://www.w3.org/Consortium/Member/Testimonial/#t1130\" title=\"Adesis Netlife S.L. Member testimonial\">testimonial</a>      </li>  \n" +
                "<li>    <a href=\"http://www.adobe.com/\" rel=\"nofollow\" class=\"member\">Adobe Systems Inc.</a>          </li>  \n";
//        string = "<li>    <a href=\"http://www.access-company.com/\" rel=\"nofollow\" class=\"member\">ACCESS CO., LTD.</a>          </li>  \n";
        System.out.println(string.matches("</a>?[\\s\\S]*?(?<=class=\"member\">)"));
//        </a>?[\s\S]*?(class="member">)
    }

    @Test
    public void test02(){
        String string = "<li>    <a href=\"http://www.access-company.com/\" rel=\"nofollow\" class=\"member\">ACCESS CO., LTD.</a>          </li>  \n" +
                "<li>    <a href=\"http://www.accessibility.nl\" rel=\"nofollow\" class=\"member\">Accessibility Foundation</a>\n" +
                "\t\t\t— <a  class=\"testimonial_link\" href=\"http://www.w3.org/Consortium/Member/Testimonial/#t745\" title=\"Accessibility Foundation Member testimonial\">testimonial</a>      </li>  \n" +
                "<li>    <a href=\"http://www.adesis.com\" rel=\"nofollow\" class=\"member\">Adesis Netlife S.L.</a>\n" +
                "\t\t\t— <a  class=\"testimonial_link\" href=\"http://www.w3.org/Consortium/Member/Testimonial/#t1130\" title=\"Adesis Netlife S.L. Member testimonial\">testimonial</a>      </li>  \n" +
                "<li>    <a href=\"http://www.adobe.com/\" rel=\"nofollow\" class=\"member\">Adobe Systems Inc.</a>          </li>  \n";
        Pattern pattern = Pattern.compile("[\\s\\S]*?class=\"member\">");
        Matcher matcher = pattern.matcher(string);
        string = matcher.replaceFirst("");

        pattern = Pattern.compile("</a>?[\\s\\S]*?(?<=class=\"member\">)");
        matcher = pattern.matcher(string);
        string = matcher.replaceAll("#");

        pattern = Pattern.compile("</a>.*");
        matcher = pattern.matcher(string);
        System.out.println(matcher.replaceAll(""));
    }

    @Test
    public void test03(){
        //抽取中文
        String str = "fff中文 测试asd";
        String regex = "([\\u4E00-\\u9FFF]+)";//[//u4E00-//u9FFF]为汉字
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while(matcher.find()){
            sb.append(matcher.group());
        }
        System.out.println(sb);
    }

    @Test
    public void test04(){
//        字符串验证
//        邮箱
        String str = "abcsg@163.com";
        String regex = "^\\w*?@\\w*?\\.\\w*?$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str.trim());
        System.out.println(matcher.matches());
    }

    @Test
    public void test05(){
//        用正则分割字符串。指以正则作为分隔符。
        String str = "fdsa abc aff tea fdsaasdf";
        String regex = "\\ba.*?\\b";
        Pattern pattern = Pattern.compile(regex);
        String[] result = pattern.split(str);
        for(String s : result){
            System.out.println(s);
        }
    }

    @Test
    public void test06(){
        Pattern pattern = Pattern.compile("正则表达式");
        Matcher matcher = pattern.matcher("正则表达式 Hello World,正则表达式 Hello World ");
        StringBuffer sbr = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sbr, "Java");
            System.out.println(sbr.toString());
        }
        matcher.appendTail(sbr);
        System.out.println(sbr.toString());
//        Java
//        Java Hello World,Java
//        Java Hello World,Java Hello World
    }

    @Test
    public void test07(){
        Pattern pattern = Pattern.compile("正则表达式");
        Matcher matcher = pattern.matcher("正则表达式 Hello World,正则表达式 Hello World ");
        String str = matcher.replaceAll("Java");
        System.out.println(str);
//        Java Hello World,Java Hello World
    }

    @Test
    public void test08(){
        Pattern pattern = Pattern.compile("a(b(c))d");
        String str = "abcd abcd";
        Matcher matcher = pattern.matcher(str);
        matcher.find();
        //matcher.group()   实际调用的是  matcher.group(0);
        System.out.println(matcher.group());
        System.out.println(matcher.groupCount());
//        第i括号内就是group(i);
        for(int i=0;i<=matcher.groupCount(); i++){
            System.out.println(matcher.group(i));
        }
//        abcd
//        2
//        abcd
//        bc
//        c
    }

    @Test
    public void test09(){
        String str = "123中文";
        for(int i=0;i<str.length();i++){
            System.out.println(str.charAt(i));
        }
    }

//    \b 不匹配符号，但是匹配下划线
    @Test
    public void test10(){
        System.out.println(isMatches("abc "));
        System.out.println(isMatches(" abc"));
        System.out.println(isMatches(" abc "));
        System.out.println(isMatches("abc."));
        System.out.println(isMatches("=abc"));
        System.out.println(isMatches("abc;"));
        System.out.println(isMatches("abc{"));
        System.out.println(isMatches("abc["));
        System.out.println(isMatches("(abc)"));
        System.out.println(isMatches("_abc"));
        System.out.println(isMatches("abc123"));
        System.out.println(isMatches("abca"));

    }

    public String isMatches(String abc){
        Pattern pattern = Pattern.compile("\\babc\\b");
        return "|"+abc+"|" + " --- " + pattern.matcher(abc).find();
    }

    @Test
    public void test12(){
        String str = "abc abc   abc\"\"";
        Pattern pattern = Pattern.compile("([^\"]*(\\\")?)+");
        System.out.println(pattern.matcher(str).matches());
    }

    @Test
    public void test13(){
        String str = " int i = 1; \n" +
                     " String str = \"ttt\";\n" +
                     " /*abcd\n" +
                     " ttt*/\n" +
                     " /**123\n" +
                     " 4321*/";
        String[] tempStrs = str.split("\\n");
        int i=0;
        Pattern pattern = Pattern.compile("/\\*{1,2}[\\s\\S]*");
        while(i<tempStrs.length){
            Matcher matcher = pattern.matcher(tempStrs[i]);
            if(matcher.find()){
                System.out.println(matcher.replaceFirst(""));
                Pattern pattern1 = Pattern.compile("[\\s\\S]*\\*/");
                while(!pattern1.matcher(tempStrs[++i]).find()){
                }
                System.out.println(pattern1.matcher(tempStrs[i]).replaceAll(""));
            }else {
                System.out.println(tempStrs[i]);
            }
            i++;
        }
    }

    @Test
    public void test14(){

        String str1 = "//fdsa";

        String str2 = " fdsa/**fdsa";
        // String str1 = "fdsa//";
        // /**fdsa*/
        /**
         *
         String str1 = "fdsa" //;
         */
    }

    @Test
    public void test15(){
        int j=2;
        for(int i=0;i<j;i++){
            j=5;
            System.out.println(i);
        }
    }
}
