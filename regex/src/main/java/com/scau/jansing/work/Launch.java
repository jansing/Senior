package com.scau.jansing.work;

import com.scau.jansing.util.CommonUtil;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jansing on 2015/10/23.
 */
public class Launch {
    public static void main(String[] args){
        String srcPath = System.getProperty("user.dir") + File.separator + "regex\\src\\main\\resources\\Test.java";
        String keywordsPath = System.getProperty("user.dir") + File.separator + "regex\\src\\main\\resources\\keywords.txt";
        String resultPath = System.getProperty("user.dir") + File.separator + "regex\\src\\main\\resources\\result.txt";
        File file = new File(srcPath);
        File keywordsFile = new File(keywordsPath);
        File resultFile = new File(resultPath);
        if(file.exists() && keywordsFile.exists()) {
            try {
                Map<String, Integer> result = analysis(file, keywordsFile);
                outputToFile(result, resultFile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else{
            System.out.println("文件不存在");
        }
    }

    /**
     * 对代码 一行一行进行分析
     * @param file
     * @param keywordsFile
     * @throws IOException
     */
    public static Map<String, Integer> analysis(File file, File keywordsFile) throws IOException {
        Map<String, Integer> keywordsMap = getKeywords(keywordsFile);
        BufferedReader bf = null;
        try{
            Pattern keywordPattern = Pattern.compile("\\b\\w+?\\b");
            bf = new BufferedReader(new FileReader(file));
            String str;
            while((str=bf.readLine())!=null){
                String tempStr = CommonUtil.removeInvalid(str,bf);
                if(tempStr==null||tempStr.length()==0)
                    continue;
                Matcher matcher = keywordPattern.matcher(tempStr);
                while (matcher.find()) {
                    Integer i = keywordsMap.get(matcher.group());
                    if(i!=null) {
                        keywordsMap.put(matcher.group(), ++i);
                    }
                }
            }
        }finally{
            if(bf!=null){
                bf.close();
            }
        }
        return keywordsMap;
    }


    /**
     * 得到关键字map
     * @param file
     * @return
     * @throws IOException
     */
    public static Map<String, Integer> getKeywords(File file) throws IOException {
        Map<String, Integer> keywordsMap = new HashMap<>();
        BufferedReader bf = new BufferedReader(new FileReader(file));
        String str = null;
        try {
            while ((str = bf.readLine()) != null) {
                String[] strings = str.split(" ");
                for (int i = 0; i < strings.length; i++) {
                    keywordsMap.put(strings[i], 0);
                }
            }
        }finally {
            if(bf!=null && bf.ready()){
                bf.close();
            }
        }
        return keywordsMap;
    }

    public static void outputToFile(Map<String, Integer> content, File file) throws IOException {
        Set<String> keySet = content.keySet();
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(file));
            for (String key : keySet) {
                bw.write(key + " : " + content.get(key) + "\n");
            }
            bw.flush();
        }finally{
            if(bw != null)
                bw.close();
        }
    }
}
