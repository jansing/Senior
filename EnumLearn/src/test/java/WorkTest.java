import learn1.SimulationGender;
import org.junit.Test;

import java.lang.reflect.Field;

/**
 * Created by jansing on 2015/11/20.
 */
public class WorkTest {

    @Test
    public void test01() throws IllegalAccessException {
        Class clazz = SimulationGender.class;
        Field[] fields = clazz.getDeclaredFields();
        for(int i=0; i<fields.length; i++){
            System.out.println(fields[i].get(clazz));
            if(fields[i].get(clazz)!=null){
                System.out.println(((SimulationGender)fields[i].get(clazz)).ordinal());
            }
        }
    }

    @Test
    public void test02(){
        System.out.println(SimulationGender.MAN.ordinal());
        System.out.println(SimulationGender.MAN.name());
    }

    @Test
    public void test03(){
        Integer[] a = new Integer[11];
        System.out.println(a.getClass());
        System.out.println(Integer.class);
//        class [Ljava.lang.Integer;
//        class java.lang.Integer
    }
}
