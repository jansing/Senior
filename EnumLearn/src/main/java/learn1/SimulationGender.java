package learn1;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * 模拟枚举类Gender及其枚举变量，以及枚举类自动生成的3个方法 values()、ordinal()、name()
 *
 * Created by jansing on 2015/11/20.
 */
public class SimulationGender {
    public static SimulationGender MAN = new SimulationGender();
    public static SimulationGender WOMAN = new SimulationGender();


    /**
     * 返回定义的静态变量
     *
     * @return
     */
    public static SimulationGender[] values() {
        try {
            Class clazz = SimulationGender.class;
            Field[] fields = clazz.getDeclaredFields();
            List<SimulationGender> resultList = new ArrayList<>();
            for (int i = 0; i < fields.length; i++) {
                resultList.add((SimulationGender) fields[i].get(SimulationGender.class));
            }
            //要使List直接转化为某个类型的数组，  aList ---->  a[]
            //不能用  (SimulationGender[]) resultList.toArray(result);
            //因为toArray返回的是Object[]，不能向下强制转换
            //可以提供一个指定类型的数组作为参数，调用下面这个方法来实现
            //此时，如果这个数组的长度小于List的长度，那么方法返回一个新的数组，否则将List的元素copy到这个数组
            //    而，如果这个数组的长度大于List的长度，那么 a[aList.size()]=null
            SimulationGender[] result = new SimulationGender[resultList.size()];
            resultList.toArray(result);
            return result;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 返回该静态变量的顺序
     *
     * @return
     */
    public int ordinal() {
        SimulationGender[] fields = values();
        for (int i = 0; i < fields.length; i++)
            if (fields[i].equals(this))
                return i + 1;
        return -1;
    }

    /**
     * 返回该静态变量的变量名
     *
     * @return
     */
    public String name() {
        try {
            Class clazz = SimulationGender.class;
            Field[] fields = clazz.getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                if (fields[i].get(clazz).equals(this)) {
                    return fields[i].getName();
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
