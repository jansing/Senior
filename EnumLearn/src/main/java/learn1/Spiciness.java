package learn1;

/**
 * Created by jansing on 2015/11/20.
 */
public enum Spiciness {
    NOT, MILD, MEDIUM, HOT, FLAMING
}
