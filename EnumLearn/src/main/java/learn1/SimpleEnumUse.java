package learn1;

/**
 *
 * Created by jansing on 2015/11/20.
 */
public class SimpleEnumUse {
    public static void main(String[] args){
//        枚举的输出
        System.out.println(Spiciness.HOT);
//        HOT
        System.out.println(Spiciness.HOT.equals("HOT"));
//        枚举类型与字符串类型比较
//        false
        System.out.println(Spiciness.HOT.toString().equals("HOT"));
//        true

//        枚举类型会自动添加方法： values()、ordinal()、name()
        for(Spiciness s : Spiciness.values()){
            System.out.println(s + ", name：" + s.name() + ", ordinal: " + s.ordinal());
//            NOT, name：NOT, ordinal: 0
//            MILD, name：MILD, ordinal: 1
//            MEDIUM, name：MEDIUM, ordinal: 2
//            HOT, name：HOT, ordinal: 3
//            FLAMING, name：FLAMING, ordinal: 4
        }

        //枚举值与枚举类关系：
        //原本以为：枚举类是枚举值的集合，就像{1..3}，包含1、2、3；
        //新发现：枚举值是 的数据类型是 枚举类， 即 枚举值 是 枚举类 的一个对象引用，这组输出的最后一个尤其说明这点
        System.out.println(Gender.MAN);
        System.out.println(Gender.MAN.name());
        System.out.println(Gender.MAN.getValue());
        System.out.println(Gender.class.equals(Gender.MAN.getClass()));
        System.out.println(Gender.MAN.MAN);
//        MAN
//        MAN
//        MAN123
//        true
//        MAN

//        new Gender("fdfd");    //编译出错，不能实例化enum
//        Gender.MAN("te");      //编译出错
    }
}
