import org.junit.Test;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Java压缩测试。
 * 包括：
 * 1、单文件压缩；
 * 2、多文件压缩，不包含文件夹；
 * 3、单文件夹压缩；
 * 4、多文件夹压缩，纯文件夹；
 * 5、混合压缩，可能包含文件及文件夹
 * 最终将以上5种合并，只留第5个
 *
 * <p>
 * 补充说明：
 * TODO 1、文件夹压缩有两种算法：深度优先与宽度优先，需比较
 * Created by jansing on 2016/7/23.
 */
public class FileToZipTest {

    public static String separatorChar = File.separator;
    public static String spotChar = ".";

    private static String basePath = "C:\\Users\\Administrator\\Desktop\\压缩文件夹";
    private static String batFile = "清除垃圾文件.bat";
    private static String javaFile = "keygen.java";
    private static String pdfFile = "Java语言规范中文版(第三版).pdf";
    private static String xlsFile = "2015E-Top招新.xls";
    private static String imgFile = "IMG_20150219_162817.jpg";
    private static String folder = "apache-tomcat-8.0.21";

    private static String outputBasePath = "C:\\Users\\Administrator\\Desktop\\压缩文件夹\\zip";
    private static String zipEXT = ".zip";

    @Test
    public void first() throws Exception {
        File[] files = new File[10];
        files[0] = new File(basePath.concat(File.separator).concat(javaFile));
        files[1] = new File(basePath.concat(File.separator).concat(batFile));
        files[2] = new File(basePath.concat(File.separator).concat(xlsFile));
        files[3] = new File(basePath.concat(File.separator).concat(imgFile));
        File zipFile = new File(outputBasePath.concat(File.separator).concat("one").concat(zipEXT));
//        putFileListToZip(files, zipFile);
//        putOneFileToZip(files[1], zipFile, "/abc");
    }

    @Test
    public void test03() throws Exception {
        toZipDP(new File("C:\\Users\\Administrator\\Desktop\\压缩文件夹\\mobile"), new File(outputBasePath.concat(File.separator).concat("one").concat(zipEXT)));
    }


    /**
     * 单文件夹压缩(深度优先)
     *
     * @param source
     * @param zipFile
     */
    public void toZipDP(File source, File zipFile) throws Exception {
        Stack<File> stack = new Stack<>();
        if(source.exists()){
            stack.push(source);
            execute(stack, source.getAbsolutePath().length(), zipFile);
        }
    }

    /**
     * 多文件/文件夹压缩(深度优先)
     * 这些文件必须在同一级目录下
     * @param sources
     * @param zipFile
     * @throws Exception
     */
    public void toZipDP(List<File> sources, File zipFile) throws Exception {
        Stack<File> stack = new Stack<>();
        sources.forEach(e -> {if (e.exists()) stack.push(e);});
        if(!stack.isEmpty()){
            execute(stack, sources.get(0).getAbsolutePath().length(), zipFile);
        }
    }

    public void execute(Stack<File> source, int basePathLength, File zipFile) throws Exception {
        InputStream inputStream = null;
        ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(zipFile));
        byte[] temp = new byte[1024];
        int readLength = -1;
        File file = null;
        File[] files = null;
        File tmp = null;
        try {
            while (!source.isEmpty()) {
                file = source.pop();
                if (file.isFile()) {
                    inputStream = new BufferedInputStream(new FileInputStream(file));
                    zipOutputStream.putNextEntry(new ZipEntry(file.getAbsolutePath().substring(basePathLength)));
                    while ((readLength = inputStream.read(temp)) != -1) {
                        zipOutputStream.write(temp, 0, readLength);
                    }
                    inputStream.close();
                } else {
                    Arrays.asList(file.listFiles()).stream().forEach(source::push);
                    zipOutputStream.putNextEntry(new ZipEntry(file.getAbsolutePath().substring(basePathLength).concat(separatorChar)));
                }
            }
        } catch (Exception e) {
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                    inputStream = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (zipOutputStream != null) {
                    zipOutputStream.close();
                    zipOutputStream = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private boolean assertFile(File file) {
        if (file.exists() && file.isFile()) {
            return true;
        }
        return false;
    }

    private boolean assertFolder(File file) {
        if (file.exists() && !file.isFile()) {
            return true;
        }
        return false;
    }

    public String getFileName(String fileName) {
        int index = fileName.lastIndexOf(separatorChar);
        if (index < 0) return fileName;
        return fileName.substring(index + 1);
    }

    public String getFileNameWithoutEXT(String fileName) {
        fileName = getFileName(fileName);
        int index = fileName.lastIndexOf(spotChar);
        if (index < 0) return fileName;
        return fileName.substring(0, index);
    }

    @Test
    public void test01() {
        System.out.println(getFileNameWithoutEXT(basePath));
        System.out.println(getFileNameWithoutEXT(javaFile));
        System.out.println(getFileNameWithoutEXT(pdfFile));
    }


}
