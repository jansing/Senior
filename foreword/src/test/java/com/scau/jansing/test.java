package com.scau.jansing;

import org.junit.Test;

/**
 * Created by jansing on 2015/10/4.
 */
public class test {

    /**
     * 这里教大家一个快速找到class文件真正所处包的方法。

     当无法确定某个类属于哪个包是   可以通过Test.class.getProtectionDomain();来查看

     例如:发生noSuchMethod异常时，但是确实有该方法，一般就是重复加载了jar包。
     * @throws Throwable
     */
    @Test
    public void finalize() throws Throwable {
        System.out.println(
                Test.class.getProtectionDomain()
        );




    }
}
