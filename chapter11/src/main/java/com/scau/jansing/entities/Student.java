package com.scau.jansing.entities;

/**
 * Created by jansing on 2015/12/27.
 */
public class Student {
    private String sno;
    private String name;

    public Student() {
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
