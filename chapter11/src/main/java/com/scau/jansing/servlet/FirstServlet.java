package com.scau.jansing.servlet;

import com.scau.jansing.entities.Student;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by jansing on 2015/12/27.
 */
@WebServlet(name = "firstServlet", urlPatterns = "/first")
public class FirstServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        resp.getWriter().print("<h1>不支持的请求</h1>");
        return;

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Student student = new Student();
        student.setSno(req.getParameter("sno"));
        student.setName(req.getParameter("name"));
        req.getServletContext().setAttribute("student", student);
        req.getRequestDispatcher("second").forward(req, resp);
        return;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }
}
