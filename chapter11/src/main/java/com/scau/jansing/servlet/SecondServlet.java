package com.scau.jansing.servlet;

import com.scau.jansing.entities.Student;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

/**
 * Created by jansing on 2015/12/27.
 */
@WebServlet(name = "SecondServlet", urlPatterns = "/second")
public class SecondServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        resp.getWriter().print("<h1>不支持的请求</h1>");
        return;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();

        Student student = (Student) req.getServletContext().getAttribute("student");

        out.println("<html>");
        out.println("<head>");
        out.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
        out.println("<title>学生信息</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h2>学生信息：</h2>");
        out.println("<table width=\"500\" border=\"1\">");


        out.println("<tr>");
        out.println("<td>学号：</td>");
        out.println("<td>"+student.getSno()+"</td>");
        out.println("</tr>");

        out.println("<tr>");
        out.println("<td>姓名：</td>");
        out.println("<td>"+student.getName()+"</td>");
        out.println("</tr>");

        out.println("</table>");
        out.println("</body>");
        out.println("</html>");

        return ;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }
}