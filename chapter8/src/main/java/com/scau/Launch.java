package com.scau;

import com.scau.jansing.utils.Container;
import com.scau.jansing.utils.HibernateUtils;
import com.scau.jansing.utils.RequestMap;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by jansing on 2015/11/18.
 */
public class Launch {
    private static Set<String> requestSet = RequestMap.getMethodSet();
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            init();
            String request;
            outputMenu();
            System.out.print("----->请选择操作 : ");
            EXIT:
            while ((request = scanner.nextLine().trim().toLowerCase()) != null && request.length() > 0) {
                try {
                    switch (request){
                        case "exit" :
                            break EXIT;
                        case "menu" :
                            outputMenu();
                            break;
                        default :
                            //todo 模拟 openSessionInView，问题关键：在这里openSession，然后可以在dao得到这个session
                            RequestMap.invoke(request, scanner);
                            break;
                    }
                    System.out.print("----->请选择操作 : ");
                } catch(NullPointerException e){
                    System.out.println("【输入的请求有误，请重新输入】");
                }finally {
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            scanner.close();
            destroy();
            System.exit(0);
        }
    }

    public static void init() throws Exception {
        try {
            //预先加载HibernateUtils，初始化配置
            Class.forName(HibernateUtils.class.getName());
            //模拟spring IOC功能
            Container.scanPackage("com.scau.jansing");
            //模拟spring mvc功能
            //todo 提供类似Container.scanPackage()
            RequestMap.addMethodMap("com.scau.jansing.controller.StudentController");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new ClassNotFoundException("类不存在，请检查");
        } catch (FileNotFoundException e) {
            //scanPackage 指定的包不存在
            e.printStackTrace();
            throw new FileNotFoundException("文件不存在，请检查");
        } catch (Exception e) {
            //IOC时有没未实例化的注入
            e.printStackTrace();
            throw e;
        }
    }

    public static void destroy() {

    }

    private static void outputMenu(){
        Iterator<String> iterator = requestSet.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
        System.out.println("exit");
        System.out.println("menu");
    }
}
