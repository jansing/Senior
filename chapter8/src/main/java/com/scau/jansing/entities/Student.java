package com.scau.jansing.entities;

import com.scau.jansing.utils.SexEnum;

import javax.persistence.*;

/**
 * Created by jansing on 2015/11/18.
 */
@Entity
@Table(name = "students")
public class Student {
    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "number")
    private String number;
    @Column(name = "name")
    private String name;
    @Column(name = "collage")
    private String collage;
    @Column(name = "major")
    private String major;
    @Column(name = "clazz")
    private String clazz;
    /**
     * 枚举类持久化需要@Enumerated这个注解，value可以ORDINAL，也可以STRING
     * ORDINAL指当前值在枚举类的排列顺序（从0开始），STRING指当前值在枚举类中的变量名
     */
    @Column(name = "sex")
    @Enumerated(value = EnumType.ORDINAL)
    private SexEnum sex;

    public Student() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCollage() {
        return collage;
    }

    public void setCollage(String collage) {
        this.collage = collage;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public SexEnum getSex() {
        return sex;
    }

    public void setSex(SexEnum sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "Student{" +
                "学号=" + number +
                ", 姓名='" + name + '\'' +
                ", 学院='" + collage + '\'' +
                ", 专业='" + major + '\'' +
                ", 班级='" + clazz + '\'' +
                ", 性别=" + sex +
                '}';
    }
}
