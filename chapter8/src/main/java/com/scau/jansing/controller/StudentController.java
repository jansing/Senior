package com.scau.jansing.controller;

import com.scau.jansing.entities.Student;
import com.scau.jansing.service.StudentService;
import com.scau.jansing.utils.Controller;
import com.scau.jansing.utils.RequestMapping;
import com.scau.jansing.utils.Resource;
import com.scau.jansing.utils.SexEnum;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Scanner;

/**
 * Created by jansing on 2015/11/18.
 */
@Controller
public class StudentController {

    @Resource
    private StudentService studentService;

    @RequestMapping("load")
    public String load(Scanner scanner){
        System.out.println();
        System.out.println();
        System.out.println("===查看学生信息===");
        System.out.print("请输入要更新的学生学号: ");
        String number = scanner.nextLine();
        Student student = studentService.load(number);
        System.out.println(student);
        return "load";
    }

    @RequestMapping("list")
    public String list(Scanner scanner) {
        System.out.println();
        System.out.println();
        System.out.println("===所有学生===");
        List<Student> studentList = studentService.list();
        for (int i = 0; i < studentList.size(); i++) {
            System.out.println("\t" + studentList.get(i));
        }
        return "list";
    }

    @RequestMapping("add")
    public String add(Scanner scanner) {
        System.out.println();
        System.out.println();
        System.out.println("===新增学生信息===");
        Student student = new Student();
        System.out.print("学号 = ");
        student.setNumber(scanner.nextLine().trim());
        System.out.print("姓名 = ");
        student.setName(scanner.nextLine().trim());
        System.out.print("学院 = ");
        student.setCollage(scanner.nextLine().trim());
        System.out.print("专业 = ");
        student.setMajor(scanner.nextLine().trim());
        System.out.print("班级 = ");
        student.setClazz(scanner.nextLine().trim());
        System.out.print("性别( ");
        SexEnum[] sexEnums = SexEnum.values();
        for(int i=0; i<sexEnums.length; i++){
            System.out.print(sexEnums[i]+" ");
        }
        System.out.print(") = ");
        String sex = scanner.nextLine().trim();
        try {
            student.setSex(SexEnum.get(sex));
        } catch (Exception e) {
            System.out.println("性别设置失败，" + e.getMessage());
        }
        studentService.add(student);
        return "add";
    }

    @RequestMapping("update")
    public String update(Scanner scanner){
        System.out.println();
        System.out.println();
        System.out.println("===更新学生信息===");
        System.out.print("请输入要更新的学生学号: ");
        String number = scanner.nextLine().trim();
        Student student = studentService.load(number);
        String newData;
        newData = _update("学号", student.getNumber(), scanner);
        if(newData!=null)
            student.setNumber(newData);
        newData = _update("姓名", student.getName(), scanner);
        if(newData!=null)
            student.setName(newData);
        newData = _update("学院", student.getCollage(), scanner);
        if(newData!=null)
            student.setCollage(newData);
        newData = _update("专业", student.getMajor(), scanner);
        if(newData!=null)
            student.setMajor(newData);
        newData = _update("班级", student.getClazz(), scanner);
        if(newData!=null)
            student.setClazz(newData);
        newData = _update("性别", student.getSex(), scanner);
        if(newData!=null)
            try {
                student.setSex(SexEnum.get(newData));
            } catch (Exception e) {
                System.out.println("性别更新失败，" + e.getMessage());
            }
        return "update";
    }

    private String _update(String cnName, Object variable, Scanner scanner){
        System.out.println(cnName + " = " + variable);
        System.out.println("是否要修改？(Y/N)");
        if("N".equals(scanner.next().trim())){
            return null;
        }
        System.out.print("请输入新" + cnName + " : ");
        return scanner.nextLine();
    }

    @RequestMapping("delete")
    public String delete(Scanner scanner){
        System.out.println();
        System.out.println();
        System.out.println("===删除学生信息===");
        System.out.print("请输入要删除的学生学号: ");
        String number = scanner.nextLine().trim();
        studentService.delete(number);
        return "delete";
    }

}
