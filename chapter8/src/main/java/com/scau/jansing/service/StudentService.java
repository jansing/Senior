package com.scau.jansing.service;

import com.scau.jansing.dao.StudentDao;
import com.scau.jansing.entities.Student;
import com.scau.jansing.utils.Bean;
import com.scau.jansing.utils.Resource;

import java.util.List;

/**
 * Created by jansing on 2015/11/19.
 */
@Bean
public class StudentService {

    @Resource
    private StudentDao studentDao;

    public void add(Student student){
        studentDao.add(student);
    }

    public void update(Student student){
        studentDao.update(student);
    }

    public void delete(String number){
        studentDao.delete(number);
    }

    public List<Student> list(){
        return studentDao.list();
    }

    public Student load(String number){
        return studentDao.load(number);
    }
}
