package com.scau.jansing.utils;

import org.apache.commons.lang.StringUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by jansing on 2015/11/18.
 */
public class CommonUtils {
    /**
     * 把第一个字母变成小写返回
     *
     * @param str
     * @return
     */
    public static String lowerTheFirstCase(String str) {
        return str.substring(0, 1).toLowerCase() + str.substring(1);
    }
}
