package com.scau.jansing.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 * Created by jansing on 2015/11/19.
 */
public class HibernateUtils {

    private final static SessionFactory SESSION_FACTORY = buildSessionFactory();

    private final static SessionFactory buildSessionFactory(){
        org.hibernate.cfg.Configuration cfg = new Configuration().configure();
        StandardServiceRegistryBuilder srb = new StandardServiceRegistryBuilder().applySettings(cfg.getProperties());
        StandardServiceRegistry sr = srb.build();
        SessionFactory sessionFactory = cfg.buildSessionFactory(sr);
        return sessionFactory;
    }

    public static SessionFactory getSessionFactory(){
        return SESSION_FACTORY;
    }

    public static Session getSession(){
        return SESSION_FACTORY.openSession();
    }

    public static void close(Session session){
        session.close();
    }
}
