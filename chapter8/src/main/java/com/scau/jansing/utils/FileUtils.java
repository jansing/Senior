package com.scau.jansing.utils;

import org.apache.commons.lang.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by jansing on 2015/11/18.
 */
public class FileUtils {
    public static String PROJECT_NAME = "chapter8";
    public static String SRC = "\\src";
    public static String MAVEN_EXTRA_PATH = "\\main\\java";

    /**
     *
     * @return 源代码根目录
     */
    public static String getRootCodePath(){
        return getCodePath(null);
    }

    /**
     *
     * @param path
     * @return 源代码目录
     */
    public static String getCodePath(String path){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(System.getProperty("user.dir"))
                .append("\\").append(PROJECT_NAME)
                .append(SRC)
                .append(MAVEN_EXTRA_PATH);
        if(StringUtils.isNotBlank(path)){
            stringBuilder.append("\\").append(path.replaceAll("\\.", "\\\\"));
        }
        return stringBuilder.toString();
    }

    /**
     * @param fileName xxx.java    aaa/sss/ddd.java
     * @return xxx   aaa/sss/ddd
     */
    public static String removeJavaEXT(String fileName) {
        return fileName.substring(0, fileName.indexOf("."));
    }

    /**
     * @param clazzFile java文件
     * @return java类规范名  aaa.sss.ddd
     */
    public static String getClazzName(File clazzFile) {
        //得到完整文件路径
        String clazzName = clazzFile.getAbsolutePath();
        //去掉.java后缀
        clazzName = removeJavaEXT(clazzName);
        //得到类的项目根路径，
        clazzName = clazzName.substring(FileUtils.getRootCodePath().length() + 1, clazzName.length());
        //将\转换成.
        return clazzName.replaceAll("\\\\", ".");
    }

}
