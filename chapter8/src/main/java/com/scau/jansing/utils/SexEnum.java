package com.scau.jansing.utils;

/**
 * Created by jansing on 2015/11/18.
 */
public enum SexEnum {
    MAN("男"), WOMAN("女"), NONE("未知");

    private String label;
    private SexEnum(String label){
        this.label = label;
    }
    public static SexEnum get(String name) throws Exception {
        SexEnum[] sexEnums = SexEnum.values();
        for(int i=0; i< SexEnum.values().length; i++){
            if(sexEnums[i].toString().equals(name)){
                return sexEnums[i];
            }
        }
        throw new Exception("没有这个性别");
    }

    @Override
    public String toString(){
        return label;
    }
}
