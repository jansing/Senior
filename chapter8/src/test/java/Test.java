import com.scau.jansing.controller.StudentController;
import com.scau.jansing.entities.Student;
import com.scau.jansing.utils.*;

import java.io.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by jansing on 2015/11/18.
 */
public class Test {
    /**
     * 扫描package下的类，并根据其注解获取其单例
     */
    @org.junit.Test
    public void testPackage(){
        String clazzName = "com.scau.jansing.controller";
        System.out.println(System.getProperty("user.dir"));
        System.out.println(System.getProperty("user.dir") + File.separator + clazzName.replaceAll("\\.", "\\\\"));
    }

    @org.junit.Test
    public void testString(){
        String fileName = "tttt.java";
        System.out.println(fileName.indexOf("."));//4
        System.out.println(fileName.substring(0,fileName.indexOf(".")));//tttt
        System.out.println(fileName.substring(fileName.indexOf(".")));//.java
        System.out.println(fileName.substring(fileName.indexOf("."), fileName.length()));//.java
    }

    @org.junit.Test
    public void test01() throws Exception {
        Container.scanPackage("com.scau.jansing");
    }

    @org.junit.Test
    public void test02(){
        List list = new ArrayList<>();
        list.add("fff");
        list.add("ggg");
        list.add("hhh");
        for(int i=0; i<list.size(); i++){
            System.out.println(list.get(i));
            if(i<3){
                list.add(i);
            }
        }
    }

    @org.junit.Test
    public void test03() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        System.out.println(Container.getClazzBeanName(Class.forName("com.scau.jansing.entities.Student")));
        System.out.println(Container.getClazzBeanName(StudentController.class));
    }

    @org.junit.Test
    public void test04(){
        System.out.println(Container.isBeanAnnotation(Controller.class));
        System.out.println(Container.isBeanAnnotation(Bean.class));
        System.out.println(Container.isBeanAnnotation(MyAnnotation.class));
    }

    @org.junit.Test
    public void test05(){
        Class clazz = StudentController.class;
        Field field = clazz.getDeclaredFields()[0];
        System.out.println(field);
        System.out.println(field.getClass());
        System.out.println(Container.getClazzBeanName(field.getClass()));
    }

    /**
     * junit 下不能直接获取控制台的输入
     * @throws IOException
     */
    @org.junit.Test
    public void test06() throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("outer : "+scanner.next());

        scanner.close();
    }

    @org.junit.Test
    public void test07() throws ClassNotFoundException {
        Class.forName("com.scau.jansing.utils.HibernateUtils");
    }

}
