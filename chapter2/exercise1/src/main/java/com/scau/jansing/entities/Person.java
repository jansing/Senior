package com.scau.jansing.entities;


import com.scau.jansing.util.Flyable;
import com.scau.jansing.util.ClassAtInterface;
import com.scau.jansing.util.MethodAtInterface;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * todo static代码块
 * todo 方法形参的注解，static final abstract
 * Created by jansing on 2015/10/7.
 */
@ClassAtInterface(value = 111, t1 = "t1value")
//public class Person<T>{
public class Person implements Flyable<Person>, Serializable{
//    public static String staticField = "static field";
//    public static final String FINAL_FIELD = "final field";
    private int a;
    private String idNo;
    private String name;
    private String sex;
    private Integer age;
    private Boolean isMerried;
    private Date born;
    private List<Integer> telephone;

    @MethodAtInterface(MethodAtInterface.Type.CONSTRUCTOR)
    public Person() throws Exception {
    }

    @MethodAtInterface(MethodAtInterface.Type.METHOD)
    public Person(String idNo) {
        this.idNo = idNo;
    }

    @MethodAtInterface(MethodAtInterface.Type.METHOD)
    public String getIdNo() {
        return idNo;
    }

    @MethodAtInterface(MethodAtInterface.Type.METHOD)
    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    @MethodAtInterface(MethodAtInterface.Type.METHOD)
    public String getName() {
        return name;
    }

    @MethodAtInterface(MethodAtInterface.Type.METHOD)
    public void setName(String name) {
        this.name = name;
    }

    @MethodAtInterface(MethodAtInterface.Type.METHOD)
    public String getSex() {
        return sex;
    }

    @MethodAtInterface(MethodAtInterface.Type.METHOD)
    public void setSex(String sex) {
        this.sex = sex;
    }

    @MethodAtInterface(MethodAtInterface.Type.METHOD)
    public Integer getAge() {
        return age;
    }

    @MethodAtInterface(MethodAtInterface.Type.METHOD)
    public void setAge(Integer age) {
        this.age = age;
    }

    @MethodAtInterface(MethodAtInterface.Type.METHOD)
    public Boolean getIsMerried() {
        return isMerried;
    }

    @MethodAtInterface(MethodAtInterface.Type.METHOD)
    public void setIsMerried(Boolean isMerried) {
        this.isMerried = isMerried;
    }

    @MethodAtInterface(MethodAtInterface.Type.METHOD)
    public Date getBorn() {
        return born;
    }

    @MethodAtInterface(MethodAtInterface.Type.METHOD)
    public void setBorn(Date born) {
        this.born = born;
    }

    @MethodAtInterface(MethodAtInterface.Type.METHOD)
    public List<Integer> getTelephone() {
        return telephone;
    }

    @MethodAtInterface(MethodAtInterface.Type.METHOD)
    public void setTelephone(List<Integer> telephone) {
        this.telephone = telephone;
    }

    @Override
    @MethodAtInterface(MethodAtInterface.Type.METHOD)
    public void fly(Person person) {
        System.out.println("flying");
    }
}
