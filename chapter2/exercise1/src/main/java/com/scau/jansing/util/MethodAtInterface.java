package com.scau.jansing.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by jansing on 2015/10/8.
 */
@Target({ElementType.CONSTRUCTOR, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MethodAtInterface {
    public enum Type{NONE, CONSTRUCTOR, METHOD};
    public Type value() default Type.NONE;

}
