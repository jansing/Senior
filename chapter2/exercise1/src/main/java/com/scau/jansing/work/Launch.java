package com.scau.jansing.work;

import com.scau.jansing.util.CommonUtils;
import com.scau.jansing.util.FileUtils;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by jansing on 2015/10/7.
 */
public class Launch {

    public static void main(String[] args) {

        try {
            //把class文件的路径加到类加载器中
            //todo 文件相对路径
//            CommonUtils.addClassPath("E:\\codes\\java\\javaSenior\\chapter2\\exercise1\\target\\classes");
            Class personClass = Class.forName("com.scau.jansing.entities.Person");
            System.out.println(Launch.getJavaFile(personClass));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getJavaFile(Class clazz) throws IOException {
        String newPackName = "create."+clazz.getPackage().getName();
                //newPackName = "create.com.scau.jansing.entities";
        try {
            StringBuilder stringBuilder = new StringBuilder();

            //得到包，并构造新包名
            stringBuilder.append("//").append(clazz.getPackage()).append(";\n");
            stringBuilder.append("package ").append(newPackName).append(";\n\n");

            //得到类注解
            Iterator<Annotation> annotationIterator = Arrays.asList(clazz.getDeclaredAnnotations()).iterator();
            while (annotationIterator.hasNext())
                stringBuilder.append(CommonUtils.getClassDeclareAnnotation(annotationIterator.next())).append("\n");

            //得到类定义
            stringBuilder.append(Modifier.toString(clazz.getModifiers())).append(" ");          //public class Person
            stringBuilder.append("class ").append(clazz.getSimpleName()).append(" ");

            //得到父类                                                                          // extends java.lang.Object
            stringBuilder.append("extends ").append(CommonUtils.removeInterfaceOrClass(clazz.getGenericSuperclass())).append("\n");

            //得到实现的接口                                                                 // implements com.scau.jansing.util.Flyable<com.scau.jansing.entities.Person>,  java.io.Serializable
            stringBuilder.append(CommonUtils.getInterfaceString(clazz.getAnnotatedInterfaces()));
            stringBuilder.append(" {").append("\n").append("\n");

            //得到属性, todo 属性初始化，根据类的权限定名得到一个实例，判断如果一个实例不是默认值，则。。。
            Iterator<Field> fieldIterator = Arrays.asList(clazz.getDeclaredFields()).iterator();
            while (fieldIterator.hasNext()) {
                Field field = fieldIterator.next();
                stringBuilder.append(CommonUtils.getTab(1))
                        .append(Modifier.toString(field.getModifiers())).append(" ")
                        .append(CommonUtils.removeInterfaceOrClass(field.getGenericType())).append(" ")
                        .append(field.getName()).append(";\n");
            }
            stringBuilder.append("\n");

            //得到构造方法
            Iterator<Constructor> constructorIterator = Arrays.asList(clazz.getConstructors()).iterator();
            while (constructorIterator.hasNext()) {
                stringBuilder.append(CommonUtils.getConstructor(constructorIterator.next())).append("\n");
            }

            //得到方法
            Iterator<Method> methodIterator = Arrays.asList(clazz.getDeclaredMethods()).iterator();
            while(methodIterator.hasNext()){
                stringBuilder.append(CommonUtils.getMethodString(methodIterator.next())).append("\n");
            }

            stringBuilder.append("}");

            return FileUtils.outputToFile(stringBuilder.toString(), newPackName+"."+clazz.getSimpleName());
        } catch (ClassNotFoundException e) {
            System.out.println("缺少文件！");
            e.printStackTrace();
        }
        return null;
    }
}
