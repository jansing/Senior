package com.scau.jansing.util;

/**
 * Created by jansing on 2015/10/8.
 */
public interface Flyable<T> {
    void fly(T t);
}
